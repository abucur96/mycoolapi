﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCoolAPI.Controllers
{
    [Area("v1")]
    [Route("api/[area]/[controller]")]
    [ApiController]
    public class UserController: ControllerBase
    {
        public UserController()
        {

        }
    }
}
