﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyCoolAPI.Models.Requests.User
{
    public class LoginRequest
    {
        [Required(AllowEmptyStrings =false, ErrorMessage = "UserName is required")]
        public virtual string UserName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Password is required")]
        public virtual string Password { get; set; }

    }
}
