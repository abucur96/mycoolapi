﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MyCoolAPI.Models.Requests.User
{
    public class RegisterRequest
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "UserName is required")]
        public virtual string UserName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Password is required")]
        public virtual string Password { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Email is required")]
        public virtual string Email { get; set; }
    }
}
