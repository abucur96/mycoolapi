﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MyCoolAPI.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MyCoolAPI.DataLayer
{
    public class MyCoolDbContext: IdentityDbContext<User>
    {
        public MyCoolDbContext(DbContextOptions<MyCoolDbContext> options): base(options)
        {

        }
    }
}
